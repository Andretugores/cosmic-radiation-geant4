# Simulación en Geant4 


Con la simulación se busca estimar la cantidad de radiación que recibe un tripulante de vuelo, en ciertas rutas de vuelo, mientras se encuentra dentro de un avión, expuesto a la radiación generada por las lluvias atmosféricas. Se realizaron cuatro simulaciones, especificadas en la tabla que se muestra a continuación, para representar dicha situación, las únicas diferencias entre los cuatro casos es la geometría y material del avión, y las dimensiones del World Volume.


<center>
![Comparacion de las simulaciones](Imagenes/simulaciones.PNG)
</center>



## Detalles generales de la simulación 

Inicialmente se creó la clase *DetectorConstruction*, a la que se ingresó la geometría de la aeronave y phantom detector. En la tabla anterior, se especifican los detalles de la geometría del avión y del World Volume para cada uno de los casos. Las aeronaves construidas con aluminio, tanto en la geometría esférica como la cilíndrica, tienen una única capa de material de espesor 4 mm. Mientras que los casos en los que las geometrías están construídas con GLARE, para poder simular el material, se crearon cinco cortezas concéntricas, cilíndricas o esféricas, dependiendo del caso, cada una de estas cortezas corresponde con uno de los materiales que conforman a GLARE, cumpliendo con las especificaciones del material definido como GLARE 3A-3. Cada una de estas capas fue creada como un sólido, el cuál tiene asociado un volumen lógico y volumen físico. Dentro y fuera de la aeronave hay aire. Para obtener mayor información sobre las definiciones de geometrías y materiales es necesario acceder a las carpetas respectivas.

Se muestran las geometrías creadas tanto para la simulación con aeronave esférica como con aeronave cilíndrica:

<center>
![Comparacion de las simulaciones](Imagenes/geometrias.PNG)
</center>

Para la detección de radiación, se creó la clase *PSEnergyDeposit*, la cual es una clase de puntuación primitiva que permite puntuar la energía de cada celda ó voxel, se define por una suma de energía dentro de la celda que calcula la energía total depositada.

## Resultados de la simulación

Las partículas que se tuvieron en cuenta para estas simulaciones son las que se encuentran en los archivos *muestra_h_BOG-BAS-esfera7m_phantom86cm-1s1.csv* y *muestra\_h\_NY-T-esfera7m\_phantom86cm-1s1.csv*
para cada conjunto de datos, definiéndolo como el generador de partículas primarias (clase *PrimaryGeneratorAction*). Las partículas y procesos físicos que están disponibles en esta aplicación se establecieron en la clase *PhysicsList*, utilizando específicamente la lista FTFP_BERT. Esta lista requiere archivos de datos para procesos electromagnéticos y hadrónicos. Se escogió FTFP_BERT debido a que es recomendada para aplicaciones de rayos cósmicos donde se requiere un buen tratamiento de partículas de muy alta energía. Sin embargo, se tiene en cuenta que no es adecuado para colisiones de energía del orden de 10 TeV o más.

Se creó la clase *RunAction* para obtener los archivos de salida, de esta clase se producen diez archivos de texto, cada uno de ellos tiene cuatro columnas, las primeras tres que corresponden a la posición del voxel en los ejes x, y, z y la última columna a la energía depositada en dicho voxel, el nombre y una descripción de los archivos obtenidos se muestra en la siguiente tabla: 

<center>
![archivos de salida](Imagenes/output-sim.PNG)
</center>

Se ejecuta la simulación siete veces, una para cada archivo producido con el archivo *procesar-data.ipynb*, archivo disponible en la carpeta *Transformacion*, los resultados obtenidos en cada ejecución se sumaran por archivos para obtener las energías depositadas totales.

Todos los archivos de salida de todas las ejecuciones de la simulación se almacenan dentro de una carpeta llamada *Resultados-simulacion* dentro de la carpeta de *Visualizacion*.

## Ejecutar la simulación

Ver apendiceA.pdf.

## Organización de la carpeta

- avion-cilindrico-aluminio: aeronave con geometría cilíndrica construída con Aluminio.
- avion-cilindrico-glare: aeronave con geometría cilíndrica construída con GLARE.
- avion-esferico-aluminio: aeronave con geometría esférica construída con Aluminio.
- avion-esferico-glare: aeronave con geometría esférica construída con GLARE.

