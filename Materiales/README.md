### Materiales

En Geant4 existen tres clases asociadas a la descripción o creación de los materiales, que serían *G4Element*, *G4Isotope* y *G4Material*. En este proyecto, se utilizó la clase *G4Material*, que describe propiedades mascroscópicas de la materia como densidad, temperatura, presión y cantidades macroscópicas relacionadas con procesos físicos como: camino libre medio, coeficientes de atenuación, transferencia lineal de energía, entre otros. Esta clase cuenta con una base de datos de materiales que recibe el nombre *NIST*, en la que se tienen elementos de la tabla periódica, moléculas, macromoléculas y componentes predefinidos. 

Los materiales de interés en estas simulaciones son el *Aluminio 2024* y *GLARE*. Ambos materiales se construyeron a partir de la fracción de masa de cada uno de sus componentes y fueron asociados a las geometrías respectivas a través de los volúmenes lógicos.

Los archivos Al2024.txt y glare.txt son ejemplos del código necesario para definir los materiales *Aluminio 2024* y *GLARE* respectivamente. 
