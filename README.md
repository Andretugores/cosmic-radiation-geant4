# Desarrollo de un modelo reproducible de simulación de dosis de secundarios de radiación cósmica en tripulación de aeronaves basado en GEANT4

TEG para optar al título de Licenciada en Física.


## Descripción del Proyecto
Conforme avanzan los años se han incrementado las investigaciones relacionadas con la exposición a la radiación de los miembros de tripulaciones aéreas durante su actividad laboral, debido a que están expuestos a niveles elevados de radiación natural de fondo provocada por la radiación cósmica. Diversos estudios han reportado mayor incidencia de cáncer, especialmente melanomas malignos y cáncer de mamas, en tripulantes de vuelo en comparación con el resto de la población, asociando este fenómeno con factores ocupacionales. En el artículo *The risk of Melanoma in Airline Pilots and Cabin Crew* evaluaron el riesgo del desarrollo de melanomas en pilotos y tripulantes de cabina, y concluyeron que tienen aproximadamente el doble de incidencia en el desarrollo de la enfermedad en comparación con la población general. Por su parte, en el artículo *Risk of breast cancer in female flight attendants: a population-based study (Iceland)* querían estudiar si había un aumento en la incidencia de cáncer de mama en los asistentes de cabina, usando la información de los trabajadores de dos compañías aéreas de Islandia, y concluyeron un exceso de cáncer de mama y melanomas malignos en comparación con el resto de la población. 

En promedio, las dosis efectivas anuales típicas de los tripulantes aéreos se estima que oscilan de 2 a 6 mSv, aunque podría variar de acuerdo a las rutas, altitudes y duraciones de los vuelos.

La Comisión Internacional para la Protección Radiológica (ICRP) establece que cualquier persona que se exponga a la radiación natural de tal manera que rebase el límite de 1 mSv al año, establecido para el público, debe ser considerado como personal ocupacionalmente expuesto (POE).

Como consecuencia, en este proyecto, es de especial interés estimar las dosis recibidas por los tripulantes aéreos durante su actividad laboral, debido a la radiación cósmica, haciendo uso de herramientas computacionales. Se proponen simulaciones basadas en Geant4, un código Monte Carlo en relación con el transporte de la radiación a través de la materia, en el caso de este trabajo, a través de un medio compuesto por varios materiales que configuran geometrías de phantom y aeronave.

Además, se tiene como objetivo resaltar la importancia de la reproducibilidad como pilar del proceso científico. Es un principio del proceso de investigación que implica la descripción detallada de los experimentos y/o códigos para que, de manera independiente, otros investigadores puedan repetirlos. Siguiendo los lineamientos de la reproducibilidad, se está generando dentro de la comunidad científica un movimiento para motivar a los investigadores a que publiquen no solo el texto y los resultados en un artículo científico, sino también sus datos, código y flujo computacional, así como cualquier otro producto intermedio.


En principio, el proyecto está construido en tres etapas. En la primera etapa se trabajó con data sintética de Lluvia Atmosférica Extendida para representar las partículas presentes a la altura de vuelo. Se le realizó un preprocesamiento y transformación de los datos con las herramientas disponibles en la carpeta *Transformacion* para obtener archivos de entrada compatibles con Geant4. En la segunda etapa se realiza la simulación en Geant4 de la interacción de las partículas con el medio material, obteniendo la energía depositada o recibida en un phantom detector. Los archivos obtenidos en la primera etapa se utilizan en cuatro simulaciones, cuyos códigos están presentes y descritos tanto en la carpeta *Ejemplos* como en las carpetas de *Geometria* y *Materiales*. Por último, la tercera etapa consiste en el procesamiento de los resultados obtenidos con la simulación, en la carpeta de *Visualizacion* se encuentran las herramientas necesarias para realizar un análisis de los resultados y su representación gráfica.

## Objetivos del Proyecto

#### Objetivo General
Desarrollar una implementación en Geant4 que permita realizar simulaciones de diferentes configuraciones de secundarios de radiación cósmica y modelos de material.

#### Objetivos Específicos
- Desarrollo de geometrías phantom y aeronave. 
- Definición de materiales a usar. 
- Preparación de un algortimo para usar datos sintéticos corsika o histogramas (densidad de flujo diferencial).
- Extracción de mapas de dosis sobre phantoms para la visualización de cortes.
- Discusión de ejemplos con efecto de neutrones secundarios del material del avión.
- Publicación de un ejemplo reproducible y documentado de los casos estudiados.

## Organización del Proyecto
Carpetas: 
- Ejemplos: código Geant4 con las cuatro simulaciones realizadas.
- Geometría: archivos con definiciones humano-interpretables y secciones de código de ejemplo en Geant4 para utilizar en la creación de las geometrías en la simulación. 
- Materiales: archivos con definiciones humano-interpretables y secciones de código de ejemplo en Geant4 para utilizar en la creación de los materiales en la simulación. 
- Transformación: los datos sintéticos iniciales son analizados y transformados en un Jupyter Notebook, disponible en esta sección, que se encarga principalmente de preprocesar la data y producir un archivo macro con datos de entrada que pueda ser interpretado por Geant4. 
- Visualización: representación gráfica de los resultados obtenidos con las simulaciones y el calculo de la dosis equivalente en dos Jupyter Notebook disponibles en esta sección.

## Herramientas de Geant4
Las versiones de Geant4 manejadas en este proyecto son la 10.6.p2 que se utilizó con Visual Studio 2019 y la 11.0.p2 que se utilizó con Linux Mint 20.2 en una máquina virtual de nombre [geantGauss01.ova](http://fisica.ciens.ucv.ve/jlopez/download/) donde Geant4 ya se encuentra instalado. 

## Resultados del Proyecto

De los cuatro modelos de simulación presentados para obtener resultados se utilizaron dos: *avion-esferico-glare* y *avion-cilindrico-glare* que corresponden a un avión esférico o cilíndrico dependiendo del caso construido con GLARE, que es el material típico con el
cual están construidos los aviones. 

Con el notebook *dosis-equivalente.ipynb* obtuvimos los calculos de dosis equivalente para estudiar la variabilidad de la dosis en función de dos criterios que serían la ruta de vuelo y la geometría de la aeronave.

Lo primero que se quería estudiar era la dependencia de la dosis equivalente con la ruta de vuelo, observar la varialidad de la dosis según regiones geomagnéticas. Para esto se utilizó la simulación constituida por un avión esférico construido con GLARE para las rutas de Bogotá - Buenos Aires y New York - Tokio. En la siguiente tabla se presentan los resultados de dosis equivalente obtenidos para cada ruta.

|  Origen    |     Destino    | Duración del vuelo (hh:mm)| Dosis equivalente (Sv) |
|:-------------:|:-------------:|:------------------:|:----------------------:|
| Bogotá        | Buenos Aires  |        5:36            |7.64 e-06|
| New York      |     Tokio     |          12:51          |3.36 e-05|

La dosis equivalente en la ruta Bogota - Buenos Aires tiene un valor aproximado de $0.0076 mSv$  mientras que en la ruta de New York - Tokio tiene un valor aproximado de $0.034  mSv$. Dosis que pueden considerarse elevadas para ser recibidas en un solo viaje. Sin embargo los resultados coinciden con lo esperado, pues se esperaba que las dosis equivalentes tuviesen un valor significativo por las regiones geográficas que se están evaluando, demostrando una alta dependecia de la dosis con la latitud y altitud.

Tambien se esperaba que el resultado de dosis equivalente fuese mayor en la ruta de New York - Tokio, lo cual podía suponerse a simple vista al comparar la cantidad de partículas presentes durante $1s$ de vuelo en cada ruta, la ruta de New York - Tokio tiene casi el doble de las partículas presentes que la ruta Bogotá - Buenos Aires, demostrando que a mayores altitudes el grado de protección de la atmósfera terrestre disminuye y que cuanto más al norte o al sur del Ecuador, más radiación es recibida producto del campo magnético de la Tierra que desvía parte de la radiación cósmica lejos del Ecuador y hacia los Polos Norte y Sur. Al igual que era de esperarse debido a que el aumento de la dosis es directamente proporcional al tiempo de exposición, a mayor tiempo de exposición mayor dosis recibida, el tiempo vuelo de la ruta New York - Tokio es aproximadamente 7h más largo que en vuelo de la ruta Bogotá - Buenos Aires.

El segundo objeto de estudio era la dependencia de la dosis equivalente con la geometría del avión. Para esto se utilizaron las simulaciones de un avión esférico y uno cilíndrico ambos construidos con GLARE para la ruta Bogotá - Buenos Aires. Los resultados de dosis equivalente se presentan en la tabla: 

|Geometría|       Ruta       | Dosis equivalente (Sv) |
|:-------:|:----:|:----------------------:|
|Esférica|Bogotá-Buenos Aires|7.64 e-06|
|Cilíndrica|Bogotá-Buenos Aires|7.63 e-06|

Los resultados de la tabla anterior no presentan información relevante debido a la manera en la que son introducidas las partículas incidentes en la simulación. Al estar todas las partículas dirigidas en línea recta verticalmente hacia abajo pierde importancia la geometría de la aeronave. Sin embargo, se considera de interés como sería la variabilidad de la dosis equivalente en función de la geometría cuando las partículas incidentes estan distribuidas por todo el espacio e impactan la aeronave desde diferentes puntos.

## Conclusiones del Proyecto

Con este trabajo se logró realizar la estimación del cálculo de la dosis equivalente que reciben pilotos y tripulantes de cabina, personal que se considera ocupacionalmente  expuesto pues se ha discutido en múltiples estudios, mencionados en el documento *TFG.pdf*, que se encuentran expuestos a niveles elevados de radiación natural de fondo producto de la radiación cósmica.  Se realizó el estudio para dos rutas de vuelo, una ruta que es de interés porque atraviesa la Anomalía Magnética del Atlántico Sur y otra por su proximidad al Polo Norte, regiones en las que se reciben  mayores cantidades de radiación. Aunque los códigos utilizados en el trabajo fueron probados con la información correspondiente a las rutas mencionadas anteriormente estos códigos sirven para realizar los calculos en cualquier otra ruta. 

Se comprobó en base a los resultados obtenidos que pilotos y tripulantes de cabina se encuentran expuestos a mayores cantidades de radiación producto de su actividad laboral en comparación al resto de la población. Llegando a recibir en rutas Polares y en un solo vuelo dosis equivalentes de hasta $33.6 \hspace{0.1cm} \mu sV$ si hablamos de los resultados obtenidos en este proyecto. Sin embargo, no se incumplen los límites establecidos por la ICRP para exposición ocupacional. 

Se comprobó tambien una alta dependecia de la dosis recibida con la latitud, altitud y duración de los vuelos. Avalando entonces las recomendaciones de la ICRP, en su Publicación 75, donde establecen que las medidas regulatorias prácticas adecuadas que se debían tomar consistían en el control de la exposición individual mediante la restricción de las horas de vuelo y la selección de las rutas.

La reproducibilidad y la replicabilidad son cruciales para el rigor científico. El interés en dejar un registro bien documentado del proyecto y la insistencia en el tema de la reproducibilidd científica recae en el hecho de que la ciencia progresa gracias a los hallazgos de un investigador o grupo investigadores que crean las bases para futuras investigaciones que avancen en la misma dirección. La ciencia abierta y reproducible acelera el progreso científico y reporta múltiples beneficios para los investigadores como el ahorro de tiempo y esfuerzo. Además, de esta manera el proceso de análisis queda perfectamente registrado, se reduce  drásticamente el riesgo de errores, y se facilita la reutilización de código para otros análisis.

Citando el artículo *“Ciencia reproducible: qué, por qué, cómo”* la reproducibilidad no debería ser vista como una obligación impuesta externamente, sino como una oportunidad de mejorar nuestra manera de hacer ciencia y aumentar la contribución de nuestros trabajos al avance científico general. Hacer ciencia reproducible trae consigo múltiples ventajas, a pesar del esfuerzo inicial que siempre conlleva aprender nuevas técnicas de trabajo.  

Durante la realización del proyecto la mayor limitante se obtuvo con la capacidad de computo lo que no permitió ejecutar las simulaciones para una mayor cantidad de rutas ni repetir una cantidad de veces suficiente las rutas estudiadas para poder realizar una estimación precisa del error. Esta misma limitante ocasionó que las muestras de datos fuesen pequeñas, correspondían a único segundo de vuelo para rutas de casi 7 y 13 horas de vuelo. El tamaño de la muestra determina principalmente el tamaño del error de muestreo. Muestras de tamaños más grandes tienden a encontrar una tasa de errores más baja. Un mayor tamaño de la muestra resulta en un resultado más exacto porque el estudio se acerca más a la realidad. En consecuencia se espera que los resultados obtenidos de dosis equivalente este sujetos a un error apreciable y de valor considerable.

Como continuación a este trabajo se propone resolver el problema asociado con la inyección de las partículas primarias para poder realizar una simulaciones más precisas y cercanas a la realidad, permitiendo así estudiar también la variabilidad de la dosis equivalente recibida con la geometría de la aeronave. Considerando interesante estudiar como sería la variación de la dosis recibida para una misma ruta si los datos se obtienen durante un tormeta geomagnética, momentos en los que la Tierra es alcanzada por mayores cantidades de radiación. Podría ser de interés cuantificar el impacto sobre el tejido biológico usando las dosis efectivas con un phantom más parecido en geometría y materiales al cuerpo humano. 

