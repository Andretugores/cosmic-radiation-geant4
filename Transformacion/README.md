# Preprocesamiento y transformación de la data

Se trabajó con data sintética de Lluvias Atmosféricas Extendidas para representar las partículas presentes a la altura de vuelo. Los archivos utilizados como datos de entrada para la simulación cuentan con ocho columnas, que corresponden con la identificación de la partícula, energía cinética en MeV, tres coordenadas del momento espacial unitario y tres coordenadas de posiciones espaciales en metros. Estos archivos reciben los nombres *muestra_h_BOG-BAS-esfera7m_phantom86cm-1s1.csv* y *muestra_h_NY-T-esfera7m_phantom86cm-1s1.csv* y contienen información sobre las partículas presentes a la altura de vuelo en rutas cercanas a las rutas de Bogotá - Buenos Aires y New York - Tokio respectivamente durante 1 s de vuelo (*Nota:* los datos utilizados no corresponde a los datos reales de las rutas de vuelo antes mencionadas. Estos datos corresponde a muestras sintéticas que recorren rutas parecidas y cercanas a la Anomalía Magnética del Atlántico Sur y al Polo Norte. Sin embargo, por comodidad al momento de nombrar las rutas y para facilitar su identificación a lo largo del trabajo las rutas recibiran los nombres de Bogotá-Buenos Aires y New York-Tokio).

Se utilizó un Jupyter Notebook, llamado *procesar-data.ipynb*, para realizar un preprocesamiento del archivo.

Lo primero que se realizó fue un análisis de los datos, estudiando las distribuciones de posición de las partículas presentes, distribuciones energéticas por partícula y distribuciones angulares por partícula para verificar que los datos no estuviesen sesgados.

El segundo paso consistió en realizar una matriz de rotación para hacer coincidir el sistema de coordenadas de CORSIKA con el de Geant4, en el Jupyter Notebook se muestran en detalle ambos sistemas de coordenadas y cómo se realizó la rotación. 

En el tercer paso, se traslada el plano de inyección de las partículas hasta la posición tangente con la parte superior de la geometría de la nave. 

Por limitaciones en el código usado para la simulación, el cuarto paso consiste en seleccionar la partícula con la cual se quiere trabajar y se calcula la energía promedio de todas las partículas de este tipo.

Por último, se genera el archivo macro en el que se especifica el nombre de la partícula, energía promedio, posición, dirección y número de partículas de cada tipo presentes junto con los comandos necesarios para que se pueda ejecutar en Geant4. El comando */run/BeamOn* indica cuántas partículas con ciertas características definidas previamente serán disparadas.

<center>
                            ![Comandos presentes en el archivo en los archivos .mac](Imagenes/comandos.PNG)
</center>

Se repiten siete veces los pasos, a partir del cuarto, para obtener los archivos macros para gammas, electrones, positrones, neutrones, muones y protones, sin considerar los piones debido a que su contribución es pequeña en comparación con el resto de las partículas.

Los siete archivos obtenidos en esta fase de preprocesamiento se crean y almacenan en la carpeta *Output* y a su vez dentro de una subcarpeta que especifica a que ruta corresponden esos datos al momento de realizar las ejecuciones del código *procesar-data.ipynb*, concluyendo así la primera etapa del proyecto.

Para generar un único archivo macro de salida, llamado *primary.mac*, que especifique el nombre, posición, dirección y energía por cada partícula junto con los comandos necesarios para la ejecución se utiliza el archivo *procesar-data2.ipynb*. Los notebooks *procesar-data.ipynb* y *procesar-data2.ipynb* son similares entre sí, las únicas diferencias aparecen al momento de obtener los archivos de salida.

### Organización de la carpeta 

- Datos: en esta subcarpeta se almacenan los datos de entrada antes de ser procesados, analizados y transformados. 
- Imagenes: subcarpeta donde se almacenan las imagenes presentes en el notebook y en el README.
- *procesar-data.ipynb*: Jupyter Notebook que se encarga de realizar el preprocesamiento y transformación de los datos.
- *procesar-data2.ipynb*: Jupyter Notebook que se encarga de realizar el preprocesamiento y transformación de los datos.
- DiccionarioG4.txt: archivo que muestra como debe ser llamada cada partícula en Geant4. 
- Output: en esta subcarpeta se almacenan los archivos *.mac* obtenidos del preprocesamiento y que se utilizarán como datos de entrada en las simulaciones.

