### Geometría

En Geant4, la categoría de geometría proporciona la capacidad de describir una estructura geométrica y propagar partículas de manera eficiente a través de ella. Se usa la clase *DetectorConstruction* para definir los parámetros geométricos de la simulación utilizando volúmenes, los cuales representan las diferentes geometrías involucradas. A cada volumen se le asocian tres objetos, el primero de la clase *G4VSolid*, el segundo de la clase *G4LogicalVolume* y el tercero de la clase *G4VPhysicalVolume*. 
  

Con la simulación se busca estimar la cantidad de radiación que recibe un tripulante de vuelo, mientras se encuentra dentro de un avión, expuesto a la radiación generada por las lluvias atmosféricas. Para este proyecto, las geometrías de interés representan la aeronave y el phantom detector.


Además, es dentro de esta categoría donde se definen parámetros de visualización, volúmenes sensibles, puntuación de cantidades físicas y filtros de partículas.

Archivos de texto presentes en el directorio: 
- aeronaveEsferica.txt: ejemplo de código para la creación de los volumenes sólido, lógico y físico para una aeronave esférica y el volumen de aire en su interior. 
- phantom.txt: ejemplo de código para la creación de los volumenes sólido, lógico y físico para un phantom cúbico. 
- visualización.txt: ejemplo de código para definir los parámetros de visualización. 
- sensitivedetector.txt: ejemplo de código para la asignación del volumen sensible al volumen lógico del phantom, creación de filtros por partícula y asignación de la cantidad física de interés y de los filtros. 
