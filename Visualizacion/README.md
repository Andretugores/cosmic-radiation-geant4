# Visualización de resultados

Esta carpeta corresponde a la tercera etapa del proyecto donde se realizó el análisis de los resultados obtenidos con la simulación. Se calculó la *dosis equivalente* recibida por el phantom detector y la visualización de los mapas de dosis absorbida. 

Para el calculo de dosis equivalente se utilizó el notebook llamado *dosis-equivalente.ipynb*. Se calculó la dosis equivalente en función de la dosis absorbida por el phantom detector y los factores de poderación de la radiación recomendados por la ICRP.

Para la extracción de los mapas de dosis se crea una representación gráfica de los resultados obtenidos con las simulaciones, se utilizó el Jupyter Notebook *vis_cortes.ipynb*. El notebook simula la deposición de energía en el phantom detector, en esta representación se muestra la energía depositada por cortes de X cm del phantom. Se visualizan dos casos, el primero son los cortes perpendiculares al haz incidente y el segundo los cortes pararlelos al haz incidente. 

Para esto se utilizaron los archivos de texto que se obtuvieron de las simulaciones, donde se tenia la energía depositada por voxel. Estos archivos de texto se convierten, dentro del mismo notebook, en un DataFrame de 100 filas por 100 columnas para cada uno de los cortes con los valores de dosis depositada en cada una de las combinaciones de valores *xy* en el caso de los cortes perpendiculares y *yz* en el caso de cortes paralelos.

Los mapas de dosis obtenidos se guardan automaticamente en la carpeta *Imagenes-energia-cortes*, cada imagen esta correctamente identificada, especificando si corresponde a cortes perpendiculares o paralelos al haz, ruta de vuelo y  geometría y material de la aeronave. 


### Organización de la carpeta 

- Imagenes: subcarpeta donde se almacenan las imagenes presentes en el README.
- dosis-equivalente.ipynb: notebook que realiza el calculo de la dosis equivalente recibida por el phantom.
- vis_cortes.ipynb: notebook que simula la deposición de energía en el phantom detector.
- Resultados-simulacion: se almacenan los archivos de salida obtenidos de las simulaciones por tipo de simulación en primera instancia y en segundo lugar por ruta de vuelo. 
- imagen-energia-cortes: se almacenan los mapas de energía obtenidos con el notebook vis_cortes.ipynb
